# imports

# datetime
import datetime

# logging
import logging

# warnings
def warn(*args, **kwargs):
    pass

import warnings
warnings.warn = warn

# numpy
import numpy as np

# pandas
import pandas as pd

# matplotlib
import matplotlib.pyplot as plt

# seaborn
import seaborn as sns

# sklearn
from sklearn.model_selection import train_test_split, validation_curve, StratifiedKFold
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score, roc_curve, auc
from sklearn.preprocessing import scale, StandardScaler
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.cross_validation import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
import itertools as its
from sklearn import svm

# graphviz
import graphviz

# scipy
import scipy
from scipy.stats import ttest_rel, wilcoxon

# debug
import pdb

# ============================================================================ #


def debug(debug=False):
    if debug == True:
        pdb.set_trace()


# ============================================================================ #

# loggingfile
"""
Logging levels

#1 - debug      (detailed info)
#2 - info       (confirmation that things according to plan 
#3 - warning    (something unexpected)
#4 - error      (some function failed application must close.)
#5 - critical	(application crashes on this)
"""
tstamp = datetime.date.today()
FORMAT = ''
LOG_FILENAME = 'dtree-' + str(tstamp) + '-final.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.INFO, format=FORMAT)

# reads csv file into pandas dataframe
sns.set()

col = ['nivel', 'etapa', 'turno', 'sexo', 'etnia', 'cond_matricula', 'responsavel', 'especial', 'transp_pub', 'status']

# csvfile
csV = 'datasets/Y2_alunos.csv'

# pandasdataframe
df = pd.read_csv(csV, usecols=col, low_memory=False)

# ============================================================================ #


def final(size = 0.3, state = 0, max = None):
    """Trabalho final."""

    # debug
    debug()

    # X and Y
    X = df[['nivel', 'etapa', 'turno', 'sexo', 'etnia', 'cond_matricula', 'responsavel', 'especial', 'transp_pub']]
    Y = df['status']
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=size, random_state=state)

    # dtree
    dtree = DecisionTreeClassifier(max_depth=max)
    dtree.fit(X_train, Y_train)
    predDT = dtree.predict(X_test)

    ind = ['ativos', 'inativos']
    arvore(dtree, ind, max)

    # Logistic Regression
    lreg = LogisticRegression(class_weight='balanced')
    lreg.fit(X_train, Y_train)
    predLR = lreg.predict(X_test)
    
    # knn
    knn = KNeighborsClassifier(n_neighbors=5, weights='uniform')
    knn.fit(X_train, Y_train)
    predKN = knn.predict(X_test)

    # nb
    nb = GaussianNB()
    nb.fit(X_train, Y_train)
    predNB = nb.predict(X_test)

    # validação cruzada estratificada
    skf = StratifiedKFold(n_splits=10, shuffle=True)


    # accuracy
    # accDT, accLR, accKN, accNB = []
    accuDT = accuracy_score(Y_test, predDT)
    accuLR = accuracy_score(Y_test, predLR)
    accuKN = accuracy_score(Y_test, predKN)
    accuNB = accuracy_score(Y_test, predNB)


    # confusion matrix
    con_matDT = confusion_matrix(Y_test, predDT)
    con_matLR = confusion_matrix(Y_test, predLR)
    con_matKN = confusion_matrix(Y_test, predKN)
    con_matNB = confusion_matrix(Y_test, predNB)

    bla = ['ativos', 'inativos']
    confusao(bla, con_matDT, 'decision_tree')
    confusao(bla, con_matLR, 'logistic_regression')
    confusao(bla, con_matKN, 'knn')
    confusao(bla, con_matNB, 'naive_bayes')

    # normalized
    norm = 2880
    confusao(bla, (con_matDT / norm), 'norm_decision_tree')
    confusao(bla, (con_matLR / norm), 'norm_logistic_regression')
    confusao(bla, (con_matKN / norm), 'norm_knn')
    confusao(bla, (con_matNB / norm), 'norm_naive_bayes')

    # cross val
    scoresDT = cross_val_score(dtree, X_test, Y_test, scoring='accuracy')
    scoresLR = cross_val_score(lreg, X_test, Y_test, scoring='accuracy')
    scoresKN = cross_val_score(knn, X_test, Y_test, scoring='accuracy')
    scoresNB = cross_val_score(nb, X_test, Y_test, scoring='accuracy')

    # scores roc curve
    scorRocDT = dtree.predict_proba(X_test)[:, 1]
    scorRocLR = lreg.decision_function(X_test)
    scorRocKN = knn.predict_proba(X_test)[:, 1]
    scorRocNB = nb.predict_proba(X_test)[:, 1]

    # roc
    fprDT, tprDT, _ = roc_curve(Y_test, scorRocDT)
    fprLR, tprLR, _ = roc_curve(Y_test, scorRocLR)
    fprKN, tprKN, _ = roc_curve(Y_test, scorRocKN)
    fprNB, tprNB, _ = roc_curve(Y_test, scorRocNB)

    # auc
    aucDT = auc(fprDT, tprDT)
    aucLR = auc(fprLR, tprLR)
    aucKN = auc(fprKN, tprKN)
    aucNB = auc(fprNB, tprNB)

    # plot roc curve
    plt.plot(fprDT, tprDT, color='green', lw=2, label='DT (area = %0.2f)' % aucDT)
    plt.plot(fprLR, tprLR, color='red', lw=2, label='LR (area = %0.2f)' % aucLR)
    plt.plot(fprKN, tprKN, color='magenta', lw=2, label='KN (area = %0.2f)' % aucKN)
    plt.plot(fprNB, tprNB, color='blue', lw=2, label='NB (area = %0.2f)' % aucNB)
    plt.plot([0, 1], [0, 1], color='black', lw=2, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.02])
    plt.xlabel('Falso Positivo')
    plt.ylabel('Verdadeiro Positivo')
    plt.title('Receiver Operating Characteristic (ROC)')
    plt.legend(loc="lower right")
    plt.savefig('img/curva_roc.png')
    plt.show()

    print('# ================================================================ #')
    print('Classifier        Scores                 Acc       Std        Var')
    print('Decision Tree:   ', np.around(scoresDT, decimals=2), "{:10.2f}".format(scoresDT.mean()), "{:10.4f}".format(np.std(scoresDT)),"{:10.4f}".format(np.var(scoresDT), "{:10.2f}".format(accuDT)))
    print('Log. Regression: ', np.around(scoresLR, decimals=2), "{:10.2f}".format(scoresLR.mean()), "{:10.4f}".format(np.std(scoresLR)),"{:10.4f}".format(np.var(scoresLR), "{:10.2f}".format(accuLR)))
    print('KNeighbors:      ', np.around(scoresKN, decimals=2), "{:10.2f}".format(scoresKN.mean()), "{:10.4f}".format(np.std(scoresKN)),"{:10.4f}".format(np.var(scoresKN), "{:10.2f}".format(accuKN)))
    print('Naive Bayes:     ', np.around(scoresNB, decimals=2), "{:10.2f}".format(scoresNB.mean()), "{:10.4f}".format(np.std(scoresNB)),"{:10.4f}".format(np.var(scoresNB), "{:10.2f}".format(accuNB)))
    print('# ================================================================ #')
    print('ttest Decision Tree x KNN')
    print("{:10.2f}".format(ttest_rel(scoresDT, scoresKN).pvalue))
    print('# ================================================================ #')
    wil = wilcoxon(scoresDT, scoresKN)
    print('Wilcoxon')
    print("{:10.2f}".format(wil.pvalue))
    print('# ================================================================ #')
    print('Acurácia:')
    print("Decision Tree:       ", "{:10.2f}".format(accuDT))
    print("Logistic Regression: ", "{:10.2f}".format(accuLR))
    print("KNeighbors:          ", "{:10.2f}".format(accuKN))
    print("Naive Bayes:         ", "{:10.2f}".format(accuNB))
    print('# ================================================================ #')

# ============================================================================ #


def confusao(index, con_matrix, tit):
    """Plots confusionx' matrix."""

    df_cm = pd.DataFrame(con_matrix, index=index, columns=index)
    plt.figure(figsize=(8, 4))
    sns.heatmap(df_cm, annot=True, annot_kws={"size": 12}, fmt='.2f', cmap=plt.cm.Blues)
    plt.title('Confusion Matrix - ' + tit)
    plt.ylabel('output class')
    plt.xlabel('target class')
    plt.xticks(rotation='horizontal')
    plt.yticks(rotation='horizontal')
    plt.savefig('img/con_matrix_' + str(tit) + '.png')
    plt.show()


# ============================================================================ #

def arvore(classif, classes, max):
    """Plot decision tree graph."""

    from subprocess import check_call
    export_graphviz(classif, out_file='img/dtree.dot', class_names=classes, max_depth=max, rounded=True)
    check_call(['dot', '-Tpng', 'img/dtree.dot', '-o', 'img/dtree.png'])


# ============================================================================ #


if __name__ == '__main__':
    # lets dance....
    final(.41, max=4)
